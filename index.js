let express = require('express');
let app = express();
let pageController = require('./server/server_page');
let noteController = require('./server/server_note');
let pgp = require('pg-promise')();
let db = pgp('postgres://postgres:somePassword@localhost:5432/notes')

const PORT = process.env.PORT || 80

app.use('/script',express.static(__dirname+'/js'))
app.use('/style',express.static(__dirname+'/css'))


pageController(app,__dirname);
noteController(app,db);

app.listen(PORT)
          