
const bodyParser = require("body-parser");

module.exports = (app,db)=>{
    let controller = new Users(db);
    app.post('/add',bodyParser.json(),controller.add.bind(controller))
    app.get('/show',bodyParser.json(),controller.show.bind(controller))
}


class Users {
    constructor(db){
        this.db = db;
    }
    add(req,res){
        let note = req.body;
        this.db.none('INSERT INTO notes(name, text) VALUES(${n},${t})',{
            n:note.name,
            t:note.text
        }).then(s=>{
            res.send(note);
        })    
    }
    show(req,res){
        res.setHeader('Content-Type','application/json');
        this.db.any('SELECT * FROM notes')
        .then(note=>res.send(note))      
    }
}