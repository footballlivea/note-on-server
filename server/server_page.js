
module.exports = (app,path)=>{
    let staticControl = new StaticController(path);
    app.get('/',staticControl.index.bind(staticControl))
}

class StaticController{
    constructor(path){
        this.path = path;
    }
    index(req,res){
        res.setHeader('Content-Type','text/html');
        res.sendFile(this.path+'/pages/index.html')
    }
}

