class Note{
    constructor(name,text){
        this.text = text;
        this.name = name;
    }
    create(n,i){
        let noteElem = document.createElement('div');
        noteElem.classList.add('note_btn');

        if(i==activeContact){
            noteElem.classList.add('active');
        }

        noteElem.dataset.index=i;
    
        let showInf = document.createElement('div');
        showInf.innerHTML=">";
        showInf.classList.add('note_btn_value');
    
        noteElem.append(`${n.name}`);
        noteElem.append(showInf);
        
       return noteElem;
    }

    showNote(){
        document.querySelector('.notes').innerHTML='';
        let elem = this.note.map((n,i)=>this.create(n,i));
        document.querySelector('.notes').append(...elem);
    }
    createInfo(n,i){
        let noteElem = document.createElement('div');
        noteElem.classList.add('text');

        if(i==activeContact){
            noteElem.classList.remove('text');
            noteElem.classList.add('activeText');
        }

        noteElem.append(`${n.text}`);
        
       return noteElem;
    }
    showInfo(){
        document.querySelector('.note_value').innerHTML='';
        let elem = this.note.map((n,i)=>this.createInfo(n,i));
        document.querySelector('.note_value').append(...elem);
    }
    update(){
        noteServices.getAllNotes().then(n=>{          
            this.note = n;
            this.showNote(this.note);
            this.showInfo(this.note);
        }); 
    }
}
    let activeContact = null;

    document.addEventListener('click',(e)=>{               
    if(!e.target.matches('.note_btn_value')) return;
    let index = e.target.parentNode.dataset.index;
    activeContact = index;
    note.showNote();
    note.showInfo();
    activeContact=null;
})
